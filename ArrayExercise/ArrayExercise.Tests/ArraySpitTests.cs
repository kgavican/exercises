using System;
using System.Diagnostics;
using Xunit;

namespace ArrayExercise.Tests
{
    public class ArraySpitTests
    {
        /*
            TEST:
            Given an int array, write a function to check if it is possible to split the array into 2 separate arrays, so that the sum of the left side will equal the sum of the right side.
             
            Examples
            In: [1,1,2,3,4,5,6]
            Out: True
            Reason: the array can be split into [1,1,2,3,4] and [5,6] -> both sides sum to 11
             
            In: [1,2,3,4,5]
            Out: False
            Reason: this array cannot be split so that one side equals the other.
        */


        [Fact]
        public void ValidateArrayForTrueTestCase()
        {
            // Arrange 
            bool expectedResult = true; // [1,1,2,3,4] and [5,6]

            int[] testSet = {
                1, 1, 2, 3, 4, 5, 6
            };

            // Act 
            bool result = ArraySplit.CanBeSubset(2, testSet);

            // Assert
            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void ValidateArrayForFalseTestCase()
        {
            // Arrange 
            bool expectedResult = false;

            int[] testSet = {
                1,2,3,4,5
            };

            // Act 
            bool result = ArraySplit.CanBeSubset(2, testSet); 

            // Assert
            Assert.Equal(expectedResult, result);

        }

        // More tests cases

        [Fact]
        public void ValidateArrayForTrueOddSumTestCase()
        {
            // Arrange 
            bool expectedResult = true; // {2,3} {5}

            int[] testSet = {
                2,5,3
            };

            // Act 
            var timer = new Stopwatch();

            bool result = ArraySplit.CanBeSubset(2, testSet);

            // Assert
            Assert.Equal(expectedResult, result);
        }


        [Fact]
        public void ValidateArrayTwoForTrueTestCase()
        {
            // Arrange 
            bool expectedResult = true; // {6,2,10,6} {18,6}


            int[] testSet = {
                6, 2, 10, 6, 6, 18
            };

            // Act 
            var timer = new Stopwatch();

            bool result = ArraySplit.CanBeSubset(2, testSet);

            // Assert
            Assert.Equal(expectedResult, result);
        }

    }
}
