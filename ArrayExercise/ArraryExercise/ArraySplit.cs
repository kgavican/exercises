﻿using System;
using System.Linq;

namespace ArrayExercise
{
    public static class ArraySplit
    {
        public static bool CanBeSubset(int numberOfSets, int[] testSet)
        {          
            int sumOfSet = testSet.Sum();

            // Sum must be divisible by desired number of sets
            if(sumOfSet % numberOfSets != 0){
                return false;
            }

            int setSumMustEqual = sumOfSet / numberOfSets;

            int numberOfElements = testSet.Length;

            // Use recurrsion to check for the sets

            return FindSubset(setSumMustEqual, numberOfElements, testSet);
        }

        private static bool FindSubset(int setSumMustEqual, int index, int[] testSet){

            // emtpy set always sums to zero
            if(setSumMustEqual == 0){
                return true;
            }

            // If no more elements left to check and we aren't looking for zero, its no good
            if(index == 0 && setSumMustEqual != 0){
                return false;
            }

            // If last element is greater than sum, then ignore it
            if (testSet[index - 1] > setSumMustEqual)
            {
                return FindSubset(setSumMustEqual, index - 1, testSet); 
            }

            // The remaining sum 
            int sumExlcudingCurrent = setSumMustEqual - testSet[index - 1];

            // do we have set if we include the current element or exclude it
            return FindSubset(setSumMustEqual, index - 1, testSet)
                || FindSubset(sumExlcudingCurrent, index - 1, testSet); 
        }
    }
}
